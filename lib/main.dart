import 'dart:async';
import 'dart:convert';

import 'package:audio_service/audio_service.dart';
import 'package:cognoro/pages/CourseMapPage.dart';
import 'package:cognoro/pages/LearningPage.dart';
import 'package:cognoro/pages/SelectCoursePage.dart';
import 'package:cognoro/pages/review/DrawQuestionPage.dart';
import 'package:flutter/material.dart';
import 'package:glassmorphism/glassmorphism.dart';
import 'package:provider/provider.dart';
import 'MyAppState.dart';
import 'models/AudioPlayerHandler.dart';
import 'pages/ProfilePage.dart';
import 'pages/LoginPage.dart';
import 'pages/ReviewPage.dart';
import 'Storage.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:badges/badges.dart' as badges;

var auth_token;
var refresh_token;
var course_id;
var course_name;
var expire;
var reviews;
late AudioPlayerHandler _audioHandler; // singleton.

//add this to state
Future<int> getReviews(authToken) async {
  final response = await http.get(Uri.parse('https://cognoro.xyz/reviews'),
    headers: <String, String>{'authorization': authToken},);

  if (response.statusCode == 200) {
    int number = jsonDecode(response.body)["reviews"];
    return number;
  } else {
    throw Exception('Failed to load card');
  }
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var storage = Storage();
  auth_token = await storage.getAuthToken();
  refresh_token = await storage.getRefreshToken();
  expire = await storage.getExpire();
  course_id = await storage.getCourseId();
  course_name = await storage.getCourseName();
  if (expire != null) {
    if (DateTime
        .now()
        .millisecondsSinceEpoch / 1000 - int.parse(expire) > 0) {
      auth_token = await storage.refreshToken(auth_token, refresh_token).catchError((e){
        return null;
      });
    }
  }
  if (auth_token != null){
    reviews = await getReviews(auth_token).catchError((e){
      return 0;
    });
  } else {
    reviews = 0;
  }
  //reviews = await getReviews(auth_token);
  _audioHandler = await AudioService.init(
    builder: () => AudioPlayerHandler(auth_token, course_id),
    config: const AudioServiceConfig(
      androidNotificationChannelId: 'xyz.cognoro.app.channel.audio',
      androidNotificationChannelName: 'Audio playback',
      androidNotificationOngoing: true,
    ),
  );
  //_audioHandler.propagate();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    String route;
    if (course_id == null) {
      route = "courses";
    } else {
      route = "/";
    }
    if (auth_token == null) {
      route = "login";
    }
    precacheImage(const AssetImage("lib/assets/bg.jpg"), context);
    return ChangeNotifierProvider(
      create: (context) => MyAppState(_audioHandler),
      child: MaterialApp(
        title: 'Cognoro',
        theme: ThemeData(
          useMaterial3: true,
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.pinkAccent),
        ),
        initialRoute: route,
        routes: {
          '/': (context) => MyHomePage(auth_token: auth_token),
          "courses": (context) => SelectCoursePage(auth_token: auth_token),
          "login": (context) => LoginPage(),
          "learn": (context) => LearningPage(auth_token: auth_token, course_id: course_id, course_name: course_name),
          "review": (context) => ReviewPage(auth_token: auth_token, course_name: course_name,),
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  final String? auth_token;
  final String? course_id;
  final String? course_name;

  const MyHomePage({super.key, this.auth_token, this.course_id, this.course_name});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    appState.reviews = reviews;
    Widget page;
    return LayoutBuilder(builder: (context, constraints) {
      switch (selectedIndex) {
        case 0:
          page = CourseMapPage(auth_token: auth_token ?? widget.auth_token, course_id: widget.course_id ?? course_id, course_name: widget.course_name ?? course_name);
          break;
        case 1:
          page = ProfilePage(auth_token: auth_token ?? widget.auth_token);
          break;
        case 2:
          page = ReviewPage(auth_token: auth_token ?? widget.auth_token, course_name: widget.course_name ?? course_name);
          break;
        default:
          throw UnimplementedError('no widget for $selectedIndex');
      }
      return Scaffold(

        body: Stack(
          children: [
          Image.asset("lib/assets/bg.jpg",
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          scale: 1,
        ),
Column(children: [
      GlassmorphicFlexContainer(
            blur: 20,
            border: 2,
            borderRadius: 20,
            linearGradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  const Color(0xFFffffff).withOpacity(0.1),
                  const Color(0xFFFFFFFF).withOpacity(0.05),
                ],
                stops: const [
                  0.1,
                  1,
                ]),
            borderGradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                const Color(0xFFffffff).withOpacity(0.5),
                const Color((0xFFFFFFFF)).withOpacity(0.5),
              ],
            ),
            child: Column(children:[
              Expanded(child: page,),
              Row(children:[
                Expanded(
                  child: GlassmorphicContainer(
                    width: double.infinity,
                    height: 65,
                    blur: 20,
                    border: 2,
                    borderRadius: 20,
                    linearGradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          const Color(0xFFffffff).withOpacity(0.1),
                          const Color(0xFFFFFFFF).withOpacity(0.05),
                        ],
                        stops: const [
                          0.1,
                          1,
                        ]),
                    borderGradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        const Color(0xFFffffff).withOpacity(0.5),
                        const Color((0xFFFFFFFF)).withOpacity(0.5),
                      ],
                    ),
                    child: Center(child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        IconButton(icon: const FaIcon(FontAwesomeIcons.map, color: Colors.white), onPressed: (){
                          setState(() {
                          selectedIndex = 0;
                        });}),
                        IconButton(icon: const Icon(Icons.person, color: Colors.white), onPressed: (){
                          setState(() {
                          selectedIndex = 1;
                        });}),
                        badges.Badge(
                          badgeContent: Text(appState.reviews.toString(),style: const TextStyle(color: Colors.white),),
                          child: IconButton(icon: const FaIcon(FontAwesomeIcons.bookSkull, color: Colors.white,), onPressed: (){
                            setState(() {
                              selectedIndex = 2;
                            });}),
                        ),
                    ],),),
                  ),),
              ]
              ),
            ]
            ),
            ),]),

          ]
        ),

      );
    });
  }
}
