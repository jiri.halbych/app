import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:http/http.dart' as http;
import 'package:synchronized/synchronized.dart';

class Storage {
  final storage = const FlutterSecureStorage();
  Duration get loginTime => const Duration(milliseconds: 2250);
  var lock = Lock();

  FlutterSecureStorage getStorage() {
    return storage;
  }

  Future<void> writeCourseId(courseid) async {
    await lock.synchronized(() async {
      await storage.write(key: 'courseid', value: courseid.toString());
    });
  }

  Future<void> writeCourseLocale(locale) async {
    await lock.synchronized(() async {
      await storage.write(key: 'locale', value: locale.replaceAll("-", "_"));
    });
  }

  Future<void> writeCourseName(name) async {
    await lock.synchronized(() async {
      await storage.write(key: 'locale', value: name);
    });
  }


  Future<void> writeFilterWriting(writing) async {
    await lock.synchronized(() async {
      await storage.write(key: 'writing', value: writing.toString());
    });
  }

  Future<String?> getAuthToken() async {
    if (await storage.containsKey(key: 'auth-token')) {
      return await storage.read(key: 'auth-token');
    } else {
      return null;
    }
  }

  Future<String?> getCourseId() async {
    if (await storage.containsKey(key: 'courseid')) {
      return await storage.read(key: 'courseid');
    } else {
      return null;
    }
  }

  Future<String?> getCourseLocale() async {
    if (await storage.containsKey(key: 'locale')) {
      return await storage.read(key: 'locale');
    } else {
      return null;
    }
  }

  Future<String?> getCourseName() async {
    if (await storage.containsKey(key: 'name')) {
      return await storage.read(key: 'name');
    } else {
      return null;
    }
  }

  Future<String?> getFilterWriting() async {
    if (await storage.containsKey(key: 'writing')) {
      return await storage.read(key: 'writing');
    } else {
      return null;
    }
  }

  Future<String?> getExpire() async {
    if (await storage.containsKey(key: 'expire')) {
      return await storage.read(key: 'expire');
    } else {
      return null;
    }
  }

  Future<String?> getRefreshToken() async {
    if (await storage.containsKey(key: 'refresh-token')) {
      return await storage.read(key: 'refresh-token');
    } else {
      return null;
    }
  }

  Future<String?> refreshToken(access, refresh) {
    return Future.delayed(loginTime).then((_) async {
      var url = Uri.https('cognoro.xyz', '/users/tokens');
      var response = await http.post(url, headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'authorization': "Bearer " + access,
        'Refresh-Token': refresh
      });
      if (response.statusCode == 200) {
        await lock.synchronized(() async {
          await storage.write(
              key: 'auth-token', value: response.headers['access-token']);
        });
        await lock.synchronized(() async {
          await storage.write(
              key: 'refresh-token', value: response.headers['refresh-token']);
        });
        await lock.synchronized(() async {
          await storage.write(
              key: 'expire', value: response.headers['expire-at']);
        });
        return response.headers['access-token'];
      } else {
        await lock.synchronized(() async {
          await storage.write(key: 'auth-token', value: null);
        });
        await lock.synchronized(() async {
          await storage.write(key: 'refresh-token', value: null);
        });
        await lock.synchronized(() async {
          await storage.write(key: 'expire', value: null);
        });
        return null;
      }
    });
  }
}
