import 'dart:async';
import 'dart:convert';

import 'package:audio_service/audio_service.dart';
import 'package:just_audio/just_audio.dart';
import 'package:http/http.dart' as http;

import 'ActivityCard.dart';

class AudioPlayerHandler extends BaseAudioHandler {
  var _player = AudioPlayer();
  late StreamSubscription<PlaybackEvent> _eventSubscription;
  var auth_token;
  var course_id;

  void setToken(token){
    auth_token = token;
  }

  void setCourseId(id){
    course_id = id;
  }

  AudioPlayerHandler(this.auth_token, this.course_id);

  List<AudioSource> getAudio(cards) {
    List<AudioSource> sources = List.empty(growable: true);
    cards.forEach((card) {
      card.audios.forEach((element) {
        sources.add(AudioSource.uri(Uri.parse(element)));
      });
    });
    return sources;
  }

  List<MediaItem> getMedia(cards) {
    List<MediaItem> sources = List.empty(growable: true);
    cards.forEach((card) {
      card.audios.forEach((element) {
        sources.add(MediaItem(id: element, title: "Dialogue"));
      });
    });
    return sources;
  }

  @override
  Future<void> playFromMediaId(String mediaId,
      [Map<String, dynamic>? extras]) async {
    propagate();

    final response = await http.get(
      Uri.parse('https://cognoro.xyz/learn?deck=$mediaId'),
      headers: <String, String>{'authorization': auth_token},
    );
    var learnCards = await ActivityCard.fromJsonDeck(jsonDecode(response.body));
    final playlist = ConcatenatingAudioSource(
      useLazyPreparation: false,
      shuffleOrder: DefaultShuffleOrder(),
      children: getAudio(learnCards),
    );
    Duration? duration = await setAudioSource(playlist);
    var item = MediaItem(id: mediaId, title: "Dialogue");
    await addQueueItems([item]);
    await addQueueItem(item);
    addItem(item);
    await _player.play();
  }

  @override
  Future<List<MediaItem>> getChildren(String parentMediaId,
      [Map<String, dynamic>? options]) async {
    //GET DECK DATA FROM API
    var response = await http.get(Uri.parse('https://cognoro.xyz/decks/audio?course=$course_id'),headers: <String, String>{'authorization': auth_token},);
    List<Deck> decks = Deck.fromJson(jsonDecode(response.body));
    List<MediaItem> seznam = List.empty(growable: true);
    for (var deck in decks) {
      seznam.add(MediaItem(id: deck.id.toString(), title: deck.name));
    }
    //List<MediaItem> seznam = [MediaItem(id: "5", title: "Audio2")];
    return seznam;
  }

  void addItem(item) {
    mediaItem.add(item);
  }

  void propagate() {
    _eventSubscription = _player.playbackEventStream.listen((event) {
      _broadcastState();
      if (event.processingState == ProcessingState.completed){
        stop();
      }
    });
  }



  AudioProcessingState _getProcessingState() {
    switch (_player.processingState) {
      case ProcessingState.idle:
        return AudioProcessingState.idle;
      case ProcessingState.loading:
        return AudioProcessingState.loading;
      case ProcessingState.buffering:
        return AudioProcessingState.buffering;
      case ProcessingState.ready:
        return AudioProcessingState.ready;
      case ProcessingState.completed:
        return AudioProcessingState.completed;
      default:
        throw Exception("Invalid state: ${_player.processingState}");
    }
  }

  void _broadcastState() {
    playbackState.add(PlaybackState(
      controls: [
        if (_player.playing) MediaControl.pause else MediaControl.play,
        MediaControl.stop,
      ],
      systemActions: const {
        MediaAction.seek,
        MediaAction.seekForward,
        MediaAction.seekBackward,
      },
      androidCompactActionIndices: [0, 1, 3],
      processingState: _getProcessingState(),
      playing: _player.playing,
      updatePosition: _player.position,
      bufferedPosition: _player.bufferedPosition,
      speed: _player.speed,
    ));
  }

  Future<Duration?> setAudioSource(source) async {
    return await _player.setAudioSource(source);
  }

  Future<Duration?> setUrl(url) async {
    return await _player.setUrl(url);
  }

  void dispose() {
    _player.dispose();
    _player = AudioPlayer();
  }

  Duration? duration() => _player.duration;
  Stream<PlaybackEvent> playbackEventStream() => _player.playbackEventStream;
  Stream<PlayerState> playerStateStream() => _player.playerStateStream;
  Stream<int?> currentIndexStream() => _player.currentIndexStream;

  @override
  Future<void> play() async {
    await _player.play();
  }

  @override
  Future<void> pause() async {
    if (_player.playing){
      await _player.pause();}
    else {
      await _player.play();
    }
  }

    @override
    Future<void> stop() async {
      await _player.stop();
      _broadcastState();
      // Shut down this task
      await super.stop();
    }
  }
