class UserCard {
  final int id;
  final String question;
  final String answer;
  final int reps;
  final DateTime due;
  final DateTime last;
  final double difficulty;
  final double stability;

  const UserCard(
      {required this.id,
        required this.question,
        required this.answer,
        required this.reps,
        required this.due,
        required this.last,
        required this.difficulty,
        required this.stability
      });

  static List<UserCard> fromJson(Map<String, dynamic> json) {
    List<UserCard> karty = List<UserCard>.empty(growable: true);
    json["cards"].forEach((card) {
      UserCard karta = UserCard(
          id: card['id'],
          question: card['question'],
          answer: card['answer'],
          reps: card['reps'],
          difficulty: card['difficulty'],
          stability: card['stability'],
          due: DateTime.parse(card['due']),
          last: DateTime.parse(card['last']),
      );
      karty.add(karta);
    });
    return karty;
  }
}