import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import '../Storage.dart';

class Deck {
  final int id;
  final String name;
  final String description;
  final bool finished;

  const Deck({
    required this.id,
    required this.name,
    required this.description,
    required this.finished
  });

  static List<Deck> fromJson(Map<String, dynamic> json) {
    List<Deck> decks = List<Deck>.empty(growable: true);
    json["data"].forEach((deck) {
      Deck balik = Deck(
          id: deck['id'],
          name: deck['name'],
          description: deck['description'],
          finished: deck['finished']
      );
      decks.add(balik);
    });
    return decks;
  }
}


class ActivityCard {
  final int id;
  final String question;
  final String answer;
  final String answer_original;
  final List<String> types;
  final List<String> audios;
  final Map<String, dynamic> options;

  const ActivityCard(
      {required this.id,
        required this.question,
        required this.answer_original,
        required this.answer,
        required this.types,
        required this.audios,
        required this.options
      });

  Future<void> sendGrade(String authToken, int grade, int id, int duration) async {
    final response = await http.post(Uri.parse('https://cognoro.xyz/repeat'),
        headers: <String, String>{'authorization': authToken, 'Content-Type': 'application/json; charset=UTF-8'},
        body: json.encode({"activity_card": id, "grade": grade, "duration": duration}));
  }

  List<TooltipSpan> getAnswer() {
    List<TooltipSpan> answers = List.empty(growable: true);
    List<String> slova = answer_original.split(' ');
    for(var i=0;i<slova.length;i++)
    {
    answers.add(TooltipSpan(inlineSpan: TextSpan(text: "${slova[i]} ", style: const TextStyle(fontSize: 20,color: Colors.white)), message: options["answer_hints"][slova[i]]));
    }
    return answers;
  }

  List<TooltipSpan> getQuestion() {
    List<TooltipSpan> questions = List.empty(growable: true);
    List<String> slova = question.split(' ');
    for(var i=0;i<slova.length;i++)
    {
      questions.add(TooltipSpan(inlineSpan: TextSpan(text: "${slova[i]} ", style: const TextStyle(fontSize: 20,color: Colors.white)), message: options["question_hints"][slova[i]]));
    }
    return questions;
  }


  factory ActivityCard.fromJson(Map<String, dynamic> json) {
    List<String> types = List.empty(growable: true);
    json["types"].forEach((type){types.add(type);});
    List<String> audios = List.empty(growable: true);
    json["audios"].forEach((audio){audios.add(audio);});
    return ActivityCard(
        id: json['id'],
        question: json['question'],
        answer_original: json['answer'],
        answer: json['answer'].replaceAll(RegExp(r'(?![ A-Za-z0-9]).'), '').replaceAll(RegExp(r' {2}'), ' ').toLowerCase(),
        types: types,
        audios: audios,
        options: jsonDecode(jsonEncode((json['options']))));
  }

  static Future<List<ActivityCard>> fromJsonDeck(Map<String, dynamic> json) async {
    Storage storage = Storage();
    String? writing = await storage.getFilterWriting();
    bool filter = true;
    if (writing != null){
      filter = bool.parse(writing);
    }
    List<ActivityCard> deck = List<ActivityCard>.empty(growable: true);
    json["cards"].forEach((card) {
    List<String> types = List.empty(growable: true);
    card["types"].forEach((type){
      if (!filter && (type == "translation" || type == "dictation")){
        } else {
          types.add(type);
      }
    });
    List<String> audios = List.empty(growable: true);
    card["audios"].forEach((audio){audios.add(audio);});
      ActivityCard karta = ActivityCard(
        id: card['id'],
        question: card['question'],
          answer_original: card['answer'],
        answer: card['answer'].replaceAll(RegExp(r'(?![ A-Za-z0-9]).'), '').replaceAll(RegExp(r' {2}'), ' ').toLowerCase(),
        types: types,
        audios: audios,
        options: jsonDecode(jsonEncode((card['options']))));
      deck.add(karta);
    });
  return deck;
  }
}

class SortCard extends ActivityCard {
  @override
  final int id;
  @override
  final String question;
  @override
  final String answer;
  @override
  final String answer_original;
  @override
  final List<String> types;
  @override
  final List<String> audios;
  final List<BigCard> items;
  @override
  final Map<String, dynamic> options;
  
  const SortCard(
      { required this.id,
        required this.question,
        required this.answer,
        required this.answer_original,
        required this.types,
        required this.audios,
        required this.items,
        required this.options}) : super(id: 0, question: '', answer: '', answer_original: "", types: const ["word_order"], audios: const [""], options: const {"a": ""});

  factory SortCard.fromActivityCard(ActivityCard card) {
    var splitted = card.answer.split(' ');
    splitted.shuffle();
    List<BigCard> items = List.empty(growable: true);
    for (var word in splitted) {
      var card = BigCard(pair: word);
      items.add(card);
    }

    return SortCard(
        id: card.id,
        question: card.question,
        answer: card.answer,
        answer_original: card.answer_original,
        types: card.types,
        items: items,
        audios: card.audios,
        options: card.options);
  }
}

class BigCard extends StatelessWidget {
  const BigCard({
    super.key,
    required this.pair,
  });

  final String pair;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Card(
      color: Colors.pinkAccent,
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Text(
          textAlign: TextAlign.center,
          overflow: TextOverflow.visible,
          maxLines: 1,
          pair,
          style: const TextStyle(
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}

class DraggingCard extends StatelessWidget {
  const DraggingCard({
    super.key,
    required this.word,
    required this.id,
    required this.funkce
  });

  final String word;
  final int id;
  final Function(DragTargetDetails)? funkce;

  @override
  Widget build(BuildContext context) {
    return DragTarget(
        builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
    ) { return Draggable<int>(
      data: id,
      feedback: Card(
        color: Colors.pinkAccent,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            style: const TextStyle(color: Colors.white, fontSize: 20),
            textAlign: TextAlign.center,
            word,
          ),
        ),
      ),
      childWhenDragging: Card(
        color: Colors.white54,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            textAlign: TextAlign.center,
            word,
            style: const TextStyle(
              color: Colors.transparent,
            ),
          ),
        ),
      ),
      child: Card(
        color: Colors.pinkAccent,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            style: const TextStyle(color: Colors.white, fontSize: 20),
            textAlign: TextAlign.center,
            word,
          ),
        ),
      ),
    );
  },onAcceptWithDetails: funkce
    );
  }
}

class TargetCard extends StatelessWidget {
  const TargetCard({
    super.key,
    required this.word,
  });

  final String word;

  @override
  Widget build(BuildContext context) {
    return DragTarget<int>(
        builder: (BuildContext context,
            List<dynamic> accepted,
            List<dynamic> rejected,) {
          return Card(
            color: Colors.white54,
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Text(
                textAlign: TextAlign.center,
                word,
                style: const TextStyle(
                  fontSize: 20,
                  color: Colors.transparent,
                ),
              ),
            ),
          );
        }
    );
  }
}

class TooltipSpan extends WidgetSpan {
  String message;
  final TextSpan inlineSpan;
  TooltipSpan({
    required this.message,
    required this.inlineSpan
  }) : super(
    child: Tooltip(
      triggerMode: TooltipTriggerMode.tap,
      onTriggered: () {
        print("$message ${inlineSpan.toPlainText()}");
      },
      message: message,
      child: Text.rich(
        inlineSpan,
      ),
    ),
  );
}

class FillCard extends ActivityCard {
  @override
  final int id;
  @override
  final String question;
  @override
  final String answer;
  @override
  final String answer_original;
  @override
  final List<String> types;
  @override
  final List<String> audios;

  @override
  final Map<String, dynamic> options;

  const FillCard(
      { required this.id,
        required this.question,
        required this.answer,
        required this.answer_original,
        required this.types,
        required this.audios,
        required this.options}) : super(id: 0, question: '', answer: '', answer_original: "", types: const ["fill"], audios: const [""], options: const {"a": ""});

  factory FillCard.fromActivityCard(ActivityCard card) {

    return FillCard(
        id: card.id,
        question: card.question,
        answer: card.answer,
        answer_original: card.answer_original,
        types: card.types,
        audios: card.audios,
        options: card.options);
  }
}
