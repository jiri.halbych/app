import 'dart:async';
import 'dart:convert';

import 'package:audio_service/audio_service.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'models/ActivityCard.dart';
import 'models/UserCard.dart';
import 'package:collection/collection.dart';

class MyAppState extends ChangeNotifier {
  var decks;
  var learn_index = 0;
  var review_index = 0;
  var activity_index = 0;
  List<int> grades = List.empty(growable: true);
  List<int> durations = List.empty(growable: true);
  var learn_cards;
  var review_cards;
  var audioHandler;
  var reviews = 0;
  MyAppState(AudioHandler audiohandler){
    audioHandler = audiohandler;
  }

  Future<int> getReviews(authToken) async {
    final response = await http.get(Uri.parse('https://cognoro.xyz/reviews'),
      headers: <String, String>{'authorization': authToken},);

    if (response.statusCode == 200) {
      reviews = jsonDecode(response.body)["reviews"];
      notifyListeners();
      return reviews;
    } else {
      throw Exception('Failed to load card');
    }
  }

  void addGrade(int grade){
    grades.add(grade);
  }

  void addDuration(int duration){
    durations.add(duration);
  }

  void increaseLearnIndex() {
    learn_index++;
    notifyListeners();
  }

  void increaseActivityIndex(authToken) {
    activity_index++;
    if (activity_index >= learn_cards[learn_index].types.length){
      learn_cards[learn_index].sendGrade(authToken, grades.average.round(), learn_cards[learn_index].id, durations.average.round());
      increaseLearnIndex();
      activity_index = 0;
    }
    notifyListeners();
  }

  void increaseReviewIndex() {
    review_index++;
    notifyListeners();
  }

  Future<List<UserCard>> fetchUserCards(authToken) async {
    final response = await http.get(Uri.parse('https://cognoro.xyz/user/cards'),
      headers: <String, String>{'authorization': authToken},);

    if (response.statusCode == 200) {
      var cards = UserCard.fromJson(jsonDecode(response.body));
      return cards;
    } else {
      throw Exception('Failed to load card');
    }
  }

  Future<List<ActivityCard>> fetchReviewDeck(authToken) async {
    final response = await http.get(Uri.parse('https://cognoro.xyz/review'),
      headers: <String, String>{'authorization': authToken},);

    if (response.statusCode == 200) {
      review_cards = await ActivityCard.fromJsonDeck(jsonDecode(response.body));
      return review_cards;
    } else {
      throw Exception('Failed to load card');
    }
  }

  Future<List<ActivityCard>> fetchLearnDeck(authToken, id) async {
    final response = await http.get(Uri.parse('https://cognoro.xyz/learn?deck=$id'), //http.get(Uri.parse('http://localhost:3000/learn?deck=$id'),
      headers: <String, String>{'authorization': authToken},);
    if (response.statusCode == 200) {
      learn_cards = await ActivityCard.fromJsonDeck(jsonDecode(response.body));
      //List<ActivityCard> copy = List.empty(growable:true);
      //learn_cards.forEach((element) {
      //   for(var i=0;i<5;i++){
      //    copy.add(element);
      //  }
      // });
      //copy.shuffle();
      //learn_cards = learn_cards + copy;
      return learn_cards;
    } else {
      throw Exception('Failed to load card');
    }
  }

  Future<List<Deck>> fetchDecks(authToken, id) async {
    final response = await http.get(Uri.parse('https://cognoro.xyz/decks?course=$id'), // http.get(Uri.parse('http://localhost:3000/decks'),
      headers: <String, String>{'authorization': authToken},);
    if (response.statusCode == 401) {
      throw 401;
    }
      if (response.statusCode == 200) {
      decks = Deck.fromJson(jsonDecode(response.body));
      return decks;
    } else {
      throw Exception('Failed to load card');
    }
  }
}