import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:just_audio/just_audio.dart';
import 'package:provider/provider.dart';
import '../../MyAppState.dart';
import '../../models/ActivityCard.dart';

class FillQuestionPage extends StatefulWidget {
  final ActivityCard card;
  final String auth_token;
  final bool learn;

  const FillQuestionPage(
      {super.key,
      required this.card,
      required this.auth_token,
      required this.learn});

  @override
  State<FillQuestionPage> createState() => _FillQuestionPageState();
}

class _FillQuestionPageState extends State<FillQuestionPage> {
  var correct;
  var current = 0;
  var orig;
  var target;
  var option1;
  var option2;
  var option3;
  var target1;
  var target2;
  var target3;
  var hotovo = false;
  List<Widget> seznam = List<Widget>.empty(growable: true);
  List<Widget> options = List<Widget>.empty(growable: true);
  List<Widget> targets = List<Widget>.empty(growable: true);

  @override
  void initState() {
    super.initState();
    List<String> moznosti = [
      widget.card.answer_original
          .split(" ")[widget.card.options["fill"]["correct"] - 1],
      ...widget.card.options["fill"]["wrong"]
    ];
    String temp =widget.card.answer_original.split(" ")[widget.card.options["fill"]["correct"] - 1];
    print(temp);
    moznosti.shuffle();
    correct = moznosti.indexOf(temp);
    print(correct);
    orig = DragTarget(
      builder: (
        BuildContext context,
        List<dynamic> accepted,
        List<dynamic> rejected,
      ) {
        return const Card(
          color: Colors.white54,
          child: SizedBox(
            width: 100,
            height: 40,
          ),
        );
      },
      onAcceptWithDetails: (data) {
        setState(() {
          current = data.data as int;
          options[current - 1] = targets[current - 1];
        });
      },
    );

    dynamic Function(DragTargetDetails)? generatorfunkce(index) {
      return (data) {
        if (current == index) {
          setState(() {
            if (current > 0) {
              options[current - 1] = seznam[current];
            }
            current = data.data as int;
            options[current - 1] = targets[current - 1];
          });
        }
      };
    }

    target = orig;
    option1 =
        DraggingCard(word: moznosti[0], id: 1, funkce: generatorfunkce(1));
    option2 =
        DraggingCard(word: moznosti[1], id: 2, funkce: generatorfunkce(2));
    option3 =
        DraggingCard(word: moznosti[2], id: 3, funkce: generatorfunkce(3));
    target1 = TargetCard(word: moznosti[0]);
    target2 = TargetCard(word: moznosti[1]);
    target3 = TargetCard(word: moznosti[2]);
    seznam = [target, option1, option2, option3];
    options = seznam.sublist(1, 4);
    targets = [target1, target2, target3];
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    final stopwatch = Stopwatch();
    stopwatch.start();
    var question = widget.card.answer_original.split(" ");
    return AbsorbPointer(
      absorbing: hotovo,
      child: Column(children: [
        Padding(
          padding: const EdgeInsets.only(bottom: 13),
          child: RichText(
            text: TextSpan(
              children: widget.card.getQuestion(),
              style: const TextStyle(
                fontSize: 20,
              ),
            ),
          ),
        ),
        Wrap(children: [
          Padding(
            padding: const EdgeInsets.only(top: 13),
            child: Text(
              question
                  .sublist(0, widget.card.options["fill"]["correct"] - 1)
                  .join(" "),
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
          seznam[current],
          Padding(
            padding: const EdgeInsets.only(top: 13),
            child: Text(
              question
                  .sublist(widget.card.options["fill"]["correct"])
                  .join(" "),
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 20,
              ),
            ),
          ),
        ]),
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Wrap(
            children: options,
          ),
        ),
        !hotovo ? ElevatedButton(
          onPressed: () async {
            setState(() {
              hotovo = true;
            });
            stopwatch.stop();
            print(current);
            print(correct);
            int grade = (correct) == (current-1) ? 3 : 1;
            ElevatedButton next = ElevatedButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  appState.addGrade(grade);
                  appState.addDuration(stopwatch.elapsedMilliseconds);
                  //widget.card.sendGrade(widget.auth_token, grade, widget.card.id, stopwatch.elapsedMilliseconds);
                  if (widget.learn) {
                    appState.increaseActivityIndex(widget.auth_token);
                  } else {
                    widget.card.sendGrade(widget.auth_token, grade,
                        widget.card.id, stopwatch.elapsedMilliseconds);
                    appState.increaseReviewIndex();
                  }
                },
                child: const Text('Pokračovat'));
            SnackBar right = SnackBar(
                duration: const Duration(days: 365),
                content: Row(children: [
                  const Icon(
                    FontAwesomeIcons.check,
                    color: Colors.greenAccent,
                  ),
                  const Text("Správná odpověď"),
                  const Spacer(),
                  next
                ]));
            SnackBar wrong = SnackBar(
                duration: const Duration(days: 365),
                content: Row(children: [
                  const Icon(
                    FontAwesomeIcons.xmark,
                    color: Colors.redAccent,
                  ),
                  Text(widget.card.answer_original
                      .split(" ")[widget.card.options["fill"]["correct"] - 1]),
                  const Spacer(),
                  next
                ]));
            ScaffoldMessenger.of(context)
                .showSnackBar(grade == 3 ? right : wrong);
            if (widget.card.options["play_at_end"]) {
              final player = AudioPlayer();
              await player.setUrl(widget.card.audios[0]);
              await player.play();
            }
          },
          child: const Text('Poslat'),
        ) : SizedBox.shrink()
      ]),
    );
  }
}
