import 'package:cognoro/main.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:just_audio/just_audio.dart';
import 'package:provider/provider.dart';
import 'package:speech_to_text/speech_recognition_event.dart';
import '../../MyAppState.dart';
import '../../Storage.dart';
import '../../models/ActivityCard.dart';
import 'package:speech_to_text/speech_to_text.dart' as stt;
import 'package:speech_to_text/speech_recognition_result.dart';

import '../LearningPage.dart';

class SpeakQuestionPage extends StatefulWidget {
  final ActivityCard card;
  final String auth_token;
  final bool learn;


  const SpeakQuestionPage({super.key, required this.card, required this.auth_token, required this.learn});

  @override
  State<SpeakQuestionPage> createState() => _SpeakQuestionPageState();
}

class _SpeakQuestionPageState extends State<SpeakQuestionPage> {
  var button;
  bool _isListening = true;
  bool recognizing = false;
  bool recognizeFinished = false;
  String text = '';
  bool _speechEnabled = false;
  String _lastWords = '';
  int grade = 1;
  var hotovo = false;
  final stt.SpeechToText _speechToText = stt.SpeechToText();

  void _onSpeechResult(SpeechRecognitionResult result) {
    setState(() {
      _lastWords = result.recognizedWords;
    });
  }

  Future<void> _stopListening() async {
    await _speechToText.stop();
    setState(() {
      recognizeFinished = true;
      recognizing = false;
    });
  }

  void _startListening(locale) async {
    setState(() {recognizeFinished=false;});
    await _speechToText.listen(onResult: _onSpeechResult, localeId: locale);
    setState(() {recognizing = true;});
  }

  void _initSpeech() async {
    var _speech = await _speechToText.initialize(
      onStatus: (val) async {
        print('onStatus: $val');
        if (val.contains("done")) {
          await _stopListening();
          setState(() {
            _isListening = false;
          });
        }
      },
    );
    setState(() {
      _speechEnabled = _speech;
    });
  }

  List<TextSpan> getAnswers(text) {
    List<TextSpan> answers = List.empty(growable: true);
    List<String> slova = text!.toLowerCase().split(' ');
    List<String> correctSlova = widget.card.answer.split(' ');
    grade = 3;
    for(var i=0;i<correctSlova.length;i++){
      if (i < slova.length){
        if (correctSlova[i] == slova[i]){
          answers.add(TextSpan(text: "${correctSlova[i]} ", style: const TextStyle(fontSize: 20, color: Colors.greenAccent)));
        } else {
          answers.add(TextSpan(text: "${correctSlova[i]} ", style: const TextStyle(fontSize: 20, color: Colors.redAccent)));
          grade = 1;
        }}
      else {
        answers.add(TextSpan(text: "${correctSlova[i]} ", style: const TextStyle(fontSize: 20, color: Colors.redAccent)));
        grade = 1;
      }
    }
    if (grade == 3 && !hotovo){
      setState(() {
        grade = 3;
        hotovo = true;
      });
      button.onPressed();
    }
    return answers;
  }

  @override
  void initState() {
    super.initState();
    _initSpeech();
  }

  @override
  void dispose(){
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var storage = Storage();
    var appState = context.watch<MyAppState>();
    final stopwatch = Stopwatch();
    stopwatch.start();
    return  AbsorbPointer(
        absorbing: hotovo,
        child: Column(children: [
      Padding(padding: const EdgeInsets.only(bottom: 13),
          child: RichText(
          text: TextSpan(
          children: widget.card.getAnswer(),
          style: const TextStyle(
            fontSize: 20,
          ),
          ),
        ),),
        if (recognizeFinished)
          //Text(_lastWords),
          _RecognizeContent(
            answers: getAnswers(_lastWords)
          ),
    Padding(padding: const EdgeInsets.only(bottom: 13),
    child: !hotovo ? ElevatedButton(
          onPressed: () async {
            if (recognizing){
              _stopListening();
            } else {
              var locale = await storage.getCourseLocale();
              _startListening(locale);
            }},
          child: recognizing
              ? const Text('Přestat nahrávat')
              : const Text('Začít nahrávat'),
        ) : SizedBox.shrink(),),
        !hotovo ? button = ElevatedButton(
          onPressed: () async {
            await Future.delayed(Duration.zero);
            setState(() {
              recognizeFinished = true;
              hotovo = true;
            });
            stopwatch.stop();
            await Future.delayed(Duration(milliseconds: 500));

            ElevatedButton next = ElevatedButton(
                onPressed: () {
                  ScaffoldMessenger.of(context).hideCurrentSnackBar();
                  appState.addGrade(grade);
                  appState.addDuration(stopwatch.elapsedMilliseconds);
                  //widget.card.sendGrade(widget.auth_token, grade, widget.card.id, stopwatch.elapsedMilliseconds);
                  setState(() {
                    _isListening = true;
                    recognizing = false;
                    recognizeFinished = false;
                    text = '';
                    _speechEnabled = false;
                    _lastWords = '';
                    hotovo = false;
                  });
                  if (widget.learn){

                    appState.increaseActivityIndex(widget.auth_token);
                  } else {
                    widget.card.sendGrade(widget.auth_token, grade, widget.card.id, stopwatch.elapsedMilliseconds);
                    appState.increaseReviewIndex();
                  }},
                child: const Text('Pokračovat')
            );
            SnackBar right = SnackBar(duration: const Duration(days: 365), content: Row(children: [const Icon(FontAwesomeIcons.check, color: Colors.greenAccent,), const Text("Správná odpověď"),const Spacer(),next]));
            SnackBar wrong = SnackBar(duration: const Duration(days: 365), content: Row(children: [const Icon(FontAwesomeIcons.cross, color: Colors.redAccent,), const Text("Špatná odpověď"),const Spacer(),next]));
            ScaffoldMessenger.of(context).showSnackBar(grade == 3 ? right : wrong);

            if (widget.card.options["play_at_end"]){
              final player = AudioPlayer();
              await player.setUrl(
                  widget.card.audios[0]);
              await player.play();
            }
          },
          child: const Text('Poslat'),
        ) : SizedBox.shrink()],
    ),);
  }
}

class _RecognizeContent extends StatelessWidget {
  final List<TextSpan> answers;

  const _RecognizeContent({required this.answers});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
          const SizedBox(
            height: 16.0,
          ),
            RichText(
              text: TextSpan(
                children: answers,
              ),
            )
        ],
      ),
    );
  }
}