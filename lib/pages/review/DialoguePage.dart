import 'package:audio_service/audio_service.dart';
import 'package:chatview/chatview.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:just_audio/just_audio.dart';
import 'package:provider/provider.dart';

import '../../MyAppState.dart';
import '../../models/ActivityCard.dart';

class DialoguePage extends StatefulWidget {
  final ActivityCard card;
  final String auth_token;
  final bool learn;

  const DialoguePage(
      {super.key,
      required this.card,
      required this.auth_token,
      required this.learn});

  @override
  State<DialoguePage> createState() => _DialoguePageState();
}

class _DialoguePageState extends State<DialoguePage> {
  Map<String, String> actors = {};
  int index = 0;
  var _audioHandler;

  final chatController = ChatController(
    initialMessageList: [],
    scrollController: ScrollController(),
    chatUsers: [],
  );

  @override
  void initState() {
    super.initState();
  }

  void createActors(List<dynamic> data) {
    var i = 1;
    for (var dialogue in data) {
      if (!actors.containsKey(dialogue["actor"])) {
        ChatUser user = ChatUser(id: '$i', name: dialogue["actor"]!);
        actors[dialogue["actor"]!] = '$i';
        chatController.chatUsers.add(user);
        i++;
      }
    }
  }

  List<Message> createMessages(List<dynamic> data) {
    List<Message> messages = List.empty(growable: true);
    for (var dialogue in data) {
      Message message = Message(
          status: MessageStatus.read,
          message: dialogue["text"]!,
          createdAt: DateTime.now(),
          sendBy: actors[dialogue["actor"]]!);
      messages.add(message);
    }
    return messages;
  }

  void onSendTap(String messagetext, ReplyMessage reply, MessageType type) {
    final message = Message(
      id: '3',
      message: messagetext,
      createdAt: DateTime.now(),
      sendBy: '1',
      replyMessage: reply,
      messageType: type,
    );
    chatController.addMessage(message);
  }

  void sendMessage(Message message) {
    chatController.addMessage(message);
  }

  List<AudioSource> getAudio() {
    List<AudioSource> sources = List.empty(growable: true);
    for (var element in widget.card.audios) {
      sources.add(AudioSource.uri(Uri.parse(element)));
    }
    return sources;
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    _audioHandler = appState.audioHandler;
    createActors(widget.card.options["dialogue"]);
    executeAfterBuild(appState);
    return ChatView(
      featureActiveConfig: const FeatureActiveConfig(enableTextField: false),
      appBar: null,
      chatBackgroundConfig:
          const ChatBackgroundConfiguration(backgroundColor: Colors.transparent),
      currentUser: ChatUser(id: '1', name: 'Flutter'),
      chatController: chatController,
      onSendTap: onSendTap,
      chatViewState:
          ChatViewState.hasMessages, // Add this state once data is available.
    );
  }

  Future<void> executeAfterBuild(appState) async {
    await Future.delayed(Duration.zero);
    ElevatedButton next = ElevatedButton(
        onPressed: () {
          appState.increaseLearnIndex();
          ScaffoldMessenger.of(context).hideCurrentSnackBar();
        },
        child: const Text('Pokračovat')
    );
    SnackBar right = SnackBar(duration: const Duration(days: 365), content: Row(children: [const Icon(FontAwesomeIcons.check, color: Colors.greenAccent,),const Spacer(),next]));

    List<Message> messages = createMessages(widget.card.options["dialogue"]);
    var audios = getAudio();
    final playlist = ConcatenatingAudioSource(
      // Start loading next item just before reaching it
      useLazyPreparation: false,
      // Customise the shuffle algorithm
      shuffleOrder: DefaultShuffleOrder(),
      // Specify the playlist items
      children: audios,
    );
    bool done = false;
    var subscription1 = _audioHandler.currentIndexStream().listen((indexx) {
      if (indexx != null) {
        sendMessage(messages[indexx]);
        if (indexx >= audios.length-1) {
          done = true;
        }
      }
    });
    _audioHandler.playerStateStream().listen((state) {
      if (state.playing){
        if (done){
          ScaffoldMessenger.of(context).showSnackBar(right);
        }
      }
      });
    await _audioHandler.addItem(const MediaItem(id: "1", title: "Dialogue"));
    await _audioHandler.setAudioSource(playlist);
    await _audioHandler.play();
  }
}
