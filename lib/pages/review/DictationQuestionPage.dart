import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:just_audio/just_audio.dart';
import 'package:provider/provider.dart';
import '../../MyAppState.dart';
import '../../models/ActivityCard.dart';


class DictationQuestionPage extends StatefulWidget {
  final ActivityCard card;
  final String auth_token;
  final bool learn;

  const DictationQuestionPage({super.key, required this.card, required this.auth_token, required this.learn});

  @override
  State<DictationQuestionPage> createState() => _DictationQuestionPageState();
}

class _DictationQuestionPageState extends State<DictationQuestionPage> {
  final myController = TextEditingController();
  var hotovo = false;
  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    final stopwatch = Stopwatch();
    stopwatch.start();
    return AbsorbPointer(
        absorbing: hotovo,
        child: Column(children: [
    Padding(padding: const EdgeInsets.only(bottom: 13),
    child:
    IconButton(icon: const FaIcon(FontAwesomeIcons.volumeHigh, color: Colors.white,), onPressed: () async {
      final player = AudioPlayer();
      await player.setUrl(
          widget.card.audios[0]);
      await player.play();
    },),),

      Padding(
        padding: const EdgeInsets.all(10.0),
        child: SizedBox(
          width: MediaQuery.of(context).size.width * 0.8,
          child: TextField(
            style: const TextStyle(color: Colors.white),
            controller: myController,
            decoration: const InputDecoration(
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 0.0),
              ),
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 0.0),
              ),
              labelStyle: TextStyle(color: Colors.white),
              focusColor: Colors.white,
              fillColor: Colors.white,
              border: OutlineInputBorder(),
              labelText: 'Answer',
            ),
          ),
        ),
      ),
      !hotovo ? ElevatedButton(
        onPressed: () async {
          setState(() {
            hotovo = true;
          });
          FocusManager.instance.primaryFocus?.unfocus();
          stopwatch.stop();
          String answer = myController.text.replaceAll(RegExp(r'(?![ A-Za-z0-9]).'), '').replaceAll(RegExp(r' {2}'), ' ').toLowerCase();
          int grade = (answer == widget.card.answer) ? 3 : 1;
          ElevatedButton next = ElevatedButton(
              onPressed: () {
                ScaffoldMessenger.of(context).hideCurrentSnackBar();
                appState.addGrade(grade);
                appState.addDuration(stopwatch.elapsedMilliseconds);
                //widget.card.sendGrade(widget.auth_token, grade, widget.card.id, stopwatch.elapsedMilliseconds);
                if (widget.learn){
                  appState.increaseActivityIndex(widget.auth_token);
                } else {
                  widget.card.sendGrade(widget.auth_token, grade, widget.card.id, stopwatch.elapsedMilliseconds);
                  appState.increaseReviewIndex();
                }},
              child: const Text('Pokračovat')
          );
          SnackBar right = SnackBar(duration: const Duration(days: 365), content: Row(children: [const Icon(FontAwesomeIcons.check, color: Colors.greenAccent,), const Text("Správná odpověď"),const Spacer(),next]));
          SnackBar wrong = SnackBar(duration: const Duration(days: 365), content: Row(children: [const Icon(FontAwesomeIcons.xmark, color: Colors.redAccent,), Text(widget.card.answer),const Spacer(),next]));
          ScaffoldMessenger.of(context).showSnackBar(grade == 3 ? right : wrong);
          if (widget.card.options["play_at_end"]){
            final player = AudioPlayer();
            await player.setUrl(
                widget.card.audios[0]);
            await player.play();
          }
          },
        child: const Text('Poslat'),
      ) : SizedBox.shrink()]
    ),);
  }
}