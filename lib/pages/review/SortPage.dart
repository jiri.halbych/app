import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:just_audio/just_audio.dart';
import 'package:provider/provider.dart';
import 'package:reorderables/reorderables.dart';
import '../../MyAppState.dart';
import '../../models/ActivityCard.dart';

class SortPage extends StatefulWidget {
  final SortCard card;
  final String auth_token;
  final bool learn;

  const SortPage({super.key, required this.card, required this.auth_token, required this.learn});

  @override
  State<SortPage> createState() => _SortPageState();
}

class _SortPageState extends State<SortPage> {
  var hotovo = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    List<BigCard> result = widget.card.items;
    final stopwatch = Stopwatch();
    stopwatch.start();
    return  AbsorbPointer(
      absorbing: hotovo,
      child: Column(children: [
    Padding(padding: const EdgeInsets.only(bottom: 13),
    child:
                  RichText(
                    text: TextSpan(
                      children: widget.card.getQuestion(),
                      style: const TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.8,
                    child: ReorderableWrap(
                      direction: Axis.horizontal,
                      needsLongPressDraggable: false,
                      spacing: 5.0,
                      runSpacing: 5.0,
                      runAlignment: WrapAlignment.spaceEvenly,
                      children: widget.card.items,
                      onReorder: (int oldIndex, int newIndex) {
                        setState(() {
                          BigCard row =
                          widget.card.items.removeAt(oldIndex);
                          widget.card.items.insert(newIndex, row);
                          var temp = result[oldIndex];
                          if (oldIndex < oldIndex) {
                            for (var i = oldIndex; i < oldIndex; i++) {
                              result[i] = result[i + 1];
                            }
                          } else {
                            for (var i = oldIndex; i > oldIndex; i--) {
                              result[i] = result[i - 1];
                            }
                          }
                          result[oldIndex] = temp;
                        });
                      },
                    ),
                  ),
                ),
                !hotovo ? ElevatedButton(
                  onPressed: () async {
                    setState(() {
                      hotovo = true;
                    });
                    stopwatch.stop();
                    int grade = 3;
                    List<String> answers = widget.card.answer.split(' ');
                    for(var i=0;i<result.length;i++){
                      if (result[i].pair != answers[i]) {
                        grade = 1;
                      }
                    }
                    ElevatedButton next = ElevatedButton(
                        onPressed: () {
                          ScaffoldMessenger.of(context).hideCurrentSnackBar();
                          appState.addGrade(grade);
                          appState.addDuration(stopwatch.elapsedMilliseconds);
                          //widget.card.sendGrade(widget.auth_token, grade, widget.card.id, stopwatch.elapsedMilliseconds);
                          if (widget.learn){
                            appState.increaseActivityIndex(widget.auth_token);
                          } else {
                            widget.card.sendGrade(widget.auth_token, grade, widget.card.id, stopwatch.elapsedMilliseconds);
                            appState.increaseReviewIndex();
                          }},
                        child: const Text('Pokračovat')
                    );
                    SnackBar right = SnackBar(duration: const Duration(days: 365), content: Row(children: [const Icon(FontAwesomeIcons.check, color: Colors.greenAccent,), const Text("Správná odpověď"),const Spacer(),next]));
                    SnackBar wrong = SnackBar(duration: const Duration(days: 365), content: Row(children: [const Icon(FontAwesomeIcons.xmark, color: Colors.redAccent,), Text(widget.card.answer),const Spacer(),next]));
                    ScaffoldMessenger.of(context).showSnackBar(grade == 3 ? right : wrong);
                    if (widget.card.options["play_at_end"]){
                      final player = AudioPlayer();
                      await player.setUrl(
                          widget.card.audios[0]);
                      await player.play();
                    }
                  },
                  child: const Text('Poslat'),
                ) : SizedBox.shrink()]
            ),);
  }
}