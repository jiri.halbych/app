import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../MyAppState.dart';
import '../../models/ActivityCard.dart';


class TheoryPage extends StatefulWidget {
  final ActivityCard card;
  final String auth_token;

  const TheoryPage({super.key, required this.card, required this.auth_token});

  @override
  State<TheoryPage> createState() => _TheoryPageState();
}

class _TheoryPageState extends State<TheoryPage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    return Column(children: [
      Padding(padding: const EdgeInsets.only(bottom: 13),
        child: Text(
            widget.card.answer_original,
            style: const TextStyle(
              fontSize: 15,
          ),
        ),
      ),
      ElevatedButton(
        onPressed: () {
          if (widget.card == appState.learn_cards[appState.learn_index]){
            appState.increaseActivityIndex(widget.auth_token);
          } else {
            appState.increaseReviewIndex();
          }
          },
        child: const Text('Pokračovat'),
      )]
    );
  }
}