import 'package:cognoro/pages/DeckPage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:provider/provider.dart';

import '../MyAppState.dart';
import '../models/ActivityCard.dart';
import 'LoginPage.dart';

class CourseMapPage extends StatefulWidget {
  final String auth_token;
  final String course_id;
  final String course_name;

  const CourseMapPage({super.key, required this.auth_token, required this.course_id, required this.course_name});

  @override
  State<CourseMapPage> createState() => _CourseMapPageState();
}

class _CourseMapPageState extends State<CourseMapPage> {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    appState.audioHandler.setToken(widget.auth_token);
    appState.audioHandler.setCourseId(widget.course_id);
    List<IconData> icons = [FontAwesomeIcons.one, FontAwesomeIcons.two, FontAwesomeIcons.three, FontAwesomeIcons.four, FontAwesomeIcons.five, FontAwesomeIcons.six, FontAwesomeIcons.seven, FontAwesomeIcons.eight, FontAwesomeIcons.nine];
    if (appState.decks != null){
      List<Widget> bubbles = List.empty(growable: true);
      int i = 0;
      appState.decks.forEach((element) {
        var bubble =  Material(type: MaterialType.transparency, child:Padding(
              padding: const EdgeInsets.all(16.0),
          child: InkWell(
            customBorder: const CircleBorder(),
            child: Ink(
              width: 90,
              height: 90,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: element.finished? Colors.greenAccent : Colors.pinkAccent),
              child: Icon(
                icons[i],
                size: 25,
                color: Colors.white,
              ),
            ),
            onTap: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => DeckPage(deck: element,auth_token: widget.auth_token, course_id: widget.course_id, course_name: widget.course_name,),
              ));
            },),
        ),);
        i++;
        bubbles.add(bubble);
      });
      return SingleChildScrollView(
        child: Center(
          child: Column(children: bubbles),
        ),
      );
    } else{
      Future<List<Deck>> futuredecks;
        futuredecks = appState.fetchDecks(widget.auth_token, widget.course_id).catchError((e){
          if (e == 401){
        Navigator.of(context).push(MaterialPageRoute(
          builder: (context) => LoginPage(),
        ));}});
      return FutureBuilder<List<Deck>>(
          future: futuredecks,
          builder: (context, snapshot) {
        if (snapshot.hasData) {
          List<Widget> bubbles = List.empty(growable: true);
          var i = 0;
          for (var element in snapshot.data!) {
            var bubble =  //GlassmorphicContainer(child:
                Material(type: MaterialType.transparency, child:Padding(
                padding: const EdgeInsets.all(16.0),
              child: InkWell(
                customBorder: const CircleBorder(),
                child: Ink(
                  width: 90,
                  height: 90,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: element.finished? Colors.greenAccent : Colors.pinkAccent),
                  child: Icon(
                    icons[i],
                    size: 25,
                    color: Colors.white,
                  ),
                ),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => DeckPage(deck: element,auth_token: widget.auth_token, course_id: widget.course_id, course_name: widget.course_name,),
                  ));
                },
            ),
            ),);
            i++;
            bubbles.add(bubble);
          }

          return SingleChildScrollView(
              child: Center(
              child: Column(
                  children: bubbles,
              )
              )
          );
        } else {
          return Center(
            child: LoadingAnimationWidget.halfTriangleDot(
              size: 200, color: Colors.pinkAccent,
            ),);        }
        });
    }
  }
}
