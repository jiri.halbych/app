import 'package:cognoro/pages/LearningPage.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:glassmorphism/glassmorphism.dart';
import 'package:provider/provider.dart';

import '../MyAppState.dart';
import '../models/ActivityCard.dart';

class DeckPage extends StatefulWidget {
  final Deck deck;
  final String auth_token;
  final String course_id;
  final String course_name;

  const DeckPage({super.key, required this.deck, required this.auth_token, required this.course_id, required this.course_name});

  @override
  State<DeckPage> createState() => _DeckPageState();
}

class _DeckPageState extends State<DeckPage> {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    var futurecards = appState.fetchLearnDeck(
        widget.auth_token, widget.deck.id);
    return Stack(children: [
      Image.asset(
        "lib/assets/bg.jpg",
        fit: BoxFit.cover,
        height: double.infinity,
        width: double.infinity,
        scale: 1,
      ),
      DefaultTabController(
        length: 2,//3,
        child: Column(children: [
          GlassmorphicFlexContainer(
            blur: 20,
            border: 2,
            borderRadius: 20,
            linearGradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  const Color(0xFFffffff).withOpacity(0.1),
                  const Color(0xFFFFFFFF).withOpacity(0.05),
                ],
                stops: const [
                  0.1,
                  1,
                ]),
            borderGradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: [
                const Color(0xFFffffff).withOpacity(0.5),
                const Color((0xFFFFFFFF)).withOpacity(0.5),
              ],
            ),
            child: LayoutBuilder(builder: (context, constraints) { return  Stack(children: [
              GlassmorphicContainer(
                  width: double.infinity,
                  height: constraints.maxWidth > 600 ? 130 : 140,
                  blur: 20,
                  border: 2,
                  borderRadius: 20,
                  linearGradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        const Color(0xFFffffff).withOpacity(0.1),
                        const Color(0xFFFFFFFF).withOpacity(0.05),
                      ],
                      stops: const [
                        0.1,
                        1,
                      ]),
                  borderGradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      const Color(0xFFffffff).withOpacity(0.5),
                      const Color((0xFFFFFFFF)).withOpacity(0.5),
                    ],
                  ),
                  child: null),
                  Scaffold(
                backgroundColor: Colors.transparent,
                appBar: PreferredSize(
                  preferredSize: constraints.maxWidth > 600 ? const Size.fromHeight(125.0) : const Size.fromHeight(100.0),
                child: AppBar(
                  iconTheme: const IconThemeData(
                    color: Colors.white, //change your color here
                  ),
                  bottom: const TabBar(
                    dividerColor: Colors.transparent,
                    tabs: [
                      Tab(
                          icon: Icon(FontAwesomeIcons.paragraph,
                              color: Colors.white)),
                      Tab(
                          icon: Icon(FontAwesomeIcons.list,
                              color: Colors.white)),
                      //Tab(
                      //    icon: Icon(FontAwesomeIcons.bookOpen,
                      //        color: Colors.white)),
                    ],
                  ),
                  title: Text(widget.deck.name,
                      style: const TextStyle(color: Colors.white)),
                  backgroundColor: Colors.transparent,
                ),),
                body: TabBarView(children: [
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Column(children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 13),
                        child: Text(
                          widget.deck.description,
                          style: const TextStyle(color: Colors.white),
                        ),),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Align(
                          alignment: Alignment.bottomRight,
                          child: ElevatedButton(
                            child: const Text("Učit se"),
                            onPressed: () {
                              Navigator.of(context)
                                  .pushReplacement(MaterialPageRoute(
                                builder: (context) =>
                                    LearningPage(
                                        auth_token: widget.auth_token, course_id: widget.course_id, course_name: widget.course_name,),
                              ));
                            },
                          ),
                        ),
                      )
                    ]),
                  ),
                  FutureBuilder(
                      future: futurecards,
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          List<DataRow> rows = List.empty(growable: true);
                          for (var element in snapshot.data!.toSet().toList()) {
                            var row = DataRow(cells: [
                              DataCell(Text(
                                element.question,
                                style: const TextStyle(color: Colors.white),
                              )),
                              DataCell(Text(
                                element.answer_original,
                                style: const TextStyle(color: Colors.white),
                              )),
                            ]);
                            rows.add(row);
                          }
                          return
                            SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: DataTable(
                            columns: const [
                              DataColumn(
                                label: Text(
                                  'Čeština',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                              DataColumn(
                                label: Text(
                                  'Cizí jazyk',
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                            rows: rows,
                          ),);
                        } else {
                          return const Placeholder();
                        }
                      }),
                //  Center(
                //  child: LoadingAnimationWidget.halfTriangleDot(
                //  size: 200, color: Colors.pinkAccent,
                //  ),),
                ]),
              ),
            ]);},),
          ),
        ]),
      ),
    ]);
  }
}