import 'package:flutter/material.dart';
import 'package:glassmorphism/glassmorphism.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:provider/provider.dart';

import '../MyAppState.dart';

class MemoryPage extends StatefulWidget {
  final String auth_token;

  const MemoryPage(
      {super.key, required this.auth_token});

  @override
  State<MemoryPage> createState() => _MemoryPageState();
}

class _MemoryPageState extends State<MemoryPage> {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    var futurecards = appState.fetchUserCards(widget.auth_token);
    return Stack(children: [
      Image.asset(
        "lib/assets/bg.jpg",
        fit: BoxFit.cover,
        height: double.infinity,
        width: double.infinity,
        scale: 1,
      ),
      Column(
          children: [GlassmorphicFlexContainer(
        blur: 20,
        border: 2,
        borderRadius: 20,
        linearGradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.bottomRight,
            colors: [
              const Color(0xFFffffff).withOpacity(0.1),
              const Color(0xFFFFFFFF).withOpacity(0.05),
            ],
            stops: const [
              0.1,
              1,
            ]),
        borderGradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.bottomRight,
          colors: [
            const Color(0xFFffffff).withOpacity(0.5),
            const Color((0xFFFFFFFF)).withOpacity(0.5),
          ],
        ),
        child: FutureBuilder(
            future: futurecards,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                List<DataRow> rows = List.empty(growable: true);
                for (var element in snapshot.data!.toSet().toList()) {
                  var stability = (((DateTime.now().difference(element.last)).inHours/24)/element.stability*100);
                  if (stability > 100){
                    stability = 100;
                  }
              var row = DataRow(cells: [
                    DataCell(Text(
                      element.answer,
                      style: const TextStyle(color: Colors.white),
                    )),
                    DataCell(Text(
                    "${(100-stability).toStringAsFixed(1)}%",//.subtract(Duration(days: element.stability.round()))).toString(),
                      style: const TextStyle(color: Colors.white),
                    )),
                  ]);
                  rows.add(row);
                }
                return Scaffold(
                    backgroundColor: Colors.transparent,
                    appBar:  AppBar(
              iconTheme: const IconThemeData(
              color: Colors.white, //change your color here
              ),
              title: const Text("Slovník",
              style: TextStyle(color: Colors.white)),
              backgroundColor: Colors.transparent,
              ),
              body:

              Row( // a dirty trick to make the DataTable fit width
                children: <Widget>[
              Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: DataTable(
                    columns: const [
                      DataColumn(
                        label: Text(
                          'Pojem',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      DataColumn(
                        label: Text(
                          'Paměť',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    ],
                    rows: rows,
                  ),
                ),),]),);
              } else {
                return Center(
                  child: LoadingAnimationWidget.halfTriangleDot(
                    size: 200,
                    color: Colors.pinkAccent,
                  ),
                );
              }
            }),
      ),]),
    ]);
  }
}
