import 'package:cognoro/pages/SelectCoursePage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_login/flutter_login.dart';
import 'package:http/http.dart' as http;
import 'package:synchronized/synchronized.dart';
import 'dart:convert';
import '../main.dart';
import '../Storage.dart';

class LoginPage extends StatelessWidget {
  LoginPage({super.key});

  Duration get loginTime => const Duration(milliseconds: 2250);
  var storage = Storage();
  var auth_token;
  var lock = Lock();

  Future<String?> _authUser(LoginData data) {
    return Future.delayed(loginTime).then((_) async {
      var url = Uri.https('cognoro.xyz', '/users/sign_in');
      var response = await http.post(url, headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      }, body: jsonEncode(<String, String?>{'email': data.name, 'password': data.password, 'password_confirmation': data.password}));
      if (response.statusCode != 200) {
        return 'Wrong login details';
      }
      await lock.synchronized(() async {
        await storage.getStorage().write(
            key: 'auth-token', value: response.headers['access-token']);
      });
      await lock.synchronized(() async {
        await storage.getStorage().write(
            key: 'refresh-token', value: response.headers['refresh-token']);
      });
      await lock.synchronized(() async {
        await storage.getStorage().write(
            key: 'expire', value: response.headers['expire-at']);
      });
      auth_token = response.headers['access-token'];
      return null;
    });
  }

  Future<String?> _signupUser(SignupData data) {
    return Future.delayed(loginTime).then((_) async {
      var url = Uri.https('cognoro.xyz', '/users/sign_up');
      var response = await http.post(url, headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      }, body: jsonEncode(<String, String?>{'email': data.name, 'password': data.password}));
      if (response.statusCode != 200) {
        return 'Wrong login details';
      }
      storage.getStorage().write(key: 'auth-token', value: response.headers['access-token']);
      storage.getStorage().write(key: 'refresh-token', value: response.headers['refresh-token']);
      storage.getStorage().write(key: 'expire', value: response.headers['expire-at']);
      auth_token = response.headers['access-token'];
      return null;
    });
  }

  Future<String?> _recoverConfirm(String error, LoginData data) {
    return Future.delayed(loginTime).then((_) async {
      var url = Uri.https('cognoro.xyz', '/users/passwords');
      var response = await http.put(url, headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      }, body: jsonEncode(<String,String?>{'password': data.password, 'password_confirmation': data.password}));
      if (response.statusCode != 200) {
        return 'Wrong Code';
      }
      return null;
    });
  }


  Future<String?> _recoverPassword(String name) {
    return Future.delayed(loginTime).then((_) async {
      var url = Uri.https('cognoro.xyz', 'reset');
      var response = await http.post(url, headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      }, body: jsonEncode(<String, Map<String?, String?>>{'user': {'email': name}}));
      if (response.statusCode != 200) {
        return 'Wrong email';
      }
      return null;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FlutterLogin(
      title: 'Aplikace',
      userType: LoginUserType.email,
      onLogin: _authUser,
      onSignup: _signupUser,
      messages: LoginMessages(loginButton: "Příhlásit", signupButton: "Registrovat", userHint: "Email", forgotPasswordButton: 'Zapomenuté heslo', passwordHint: "Heslo", confirmPasswordHint: "Heslo znovu"),
      //onConfirmRecover: _recoverConfirm,
      hideForgotPasswordButton: true,
      onSubmitAnimationCompleted: () async {
        var token = await storage.getAuthToken();
        if (await storage.getCourseId() == null){
          Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (context) => SelectCoursePage(auth_token: token!),
          ));
        }else{
        Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (context) => MyHomePage(auth_token: token),
        ));
        }
      },
      onRecoverPassword: _recoverPassword,
    );
  }
}