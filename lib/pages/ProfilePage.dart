import 'dart:async';
import 'dart:convert';

import 'package:cognoro/pages/SelectCoursePage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:loading_animation_widget/loading_animation_widget.dart';
import '../Storage.dart';
import 'MemoryPage.dart';

Future<User> fetchUser(authToken) async {
  final response = await http
      .get(Uri.parse('https://cognoro.xyz/user/info'), headers: <String, String>{'authorization': authToken});

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    return User.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load user');
  }
}

class User {
  final String email;
  final int repetitions;

  const User({
    required this.email,
    required this.repetitions,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      email: json['email'],
      repetitions: json['repetitions'],
    );
  }
}

class ProfilePage extends StatefulWidget {
  final String auth_token;
  const ProfilePage(
      {super.key, required this.auth_token});

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  late Timer t;
  bool writing = true;
  Storage storage = Storage();
  late Future<User> futureUser;
  late Future<String?> futureWriting;
  @override
  void initState() {
    super.initState();
    futureUser = fetchUser(widget.auth_token).catchError((e){
      return null;
    });
  }

  @override
  void dispose(){
    t.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    futureWriting = storage.getFilterWriting();
    return FutureBuilder<List<dynamic>>(
        future: Future.wait([futureUser, futureWriting]),
        builder: (context, snapshot) {
      if (snapshot.hasData) {
        User user = snapshot.data![0];
        if (snapshot.data![1] != null){
          t = Timer(Duration.zero, () async {
            setState(() {
              writing = bool.parse(snapshot.data![1]);
            });
          });
        }
        //return Text(snapshot.data!.email);
        return Container(
          alignment: Alignment.topCenter,
          child: SingleChildScrollView(
              child: Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Column(
            children: [
              Text("Email: ${user.email}", style: const TextStyle(color: Colors.white,
                  fontSize: 25)),
              Text("Počet splněných opakování: ${user.repetitions}", style: const TextStyle(color: Colors.white,
                  fontSize: 25)),
              ElevatedButton(
                  child: const Text("Slovník"),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => MemoryPage(
                        auth_token: widget.auth_token,
                      ),
                    ));}),
              ElevatedButton(
                  child: const Text("Změnit kurz"),
                  onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => SelectCoursePage(
              auth_token: widget.auth_token,
              ),
              ));}),
                  Row( children: [
                    Text("Povolit psací aktivity: ", style: TextStyle(color: Colors.white, fontSize: 20)),
                  Switch(
                    // This bool value toggles the switch.
                    value: writing,
                    activeColor: Colors.greenAccent,
                    onChanged: (bool value) async {
                      await storage.writeFilterWriting(value);
                      // This is called when the user toggles the switch.
                      setState(() {
                        writing = value;
                      });
                    },
                  ),]),
            ]),
              ],
            ),),
          ),
        );
      } else {
        return Center(
          child: LoadingAnimationWidget.halfTriangleDot(
            size: 200, color: Colors.pinkAccent,
          ),);
      }
      // By default, show a loading spinner.

    });
  }
}
