import 'package:cognoro/pages/review/DialoguePage.dart';
import 'package:cognoro/pages/review/DictationQuestionPage.dart';
import 'package:cognoro/pages/review/FillQuestionPage.dart';
import 'package:cognoro/pages/review/SimpleQuestionPage.dart';
import 'package:cognoro/pages/review/SortPage.dart';
import 'package:cognoro/pages/review/SpeakQuestionPage.dart';
import 'package:flutter/material.dart';
import 'package:glassmorphism/glassmorphism.dart';
import 'package:provider/provider.dart';
import '../MyAppState.dart';
import '../main.dart';
import '../models/ActivityCard.dart';
import 'package:linear_progress_bar/linear_progress_bar.dart';

class LearningPage extends StatefulWidget {
  final String auth_token;
  final String course_id;
  final String course_name;

  const LearningPage(
      {super.key, required this.auth_token, required this.course_id, required this.course_name });

  @override
  State<LearningPage> createState() => _LearningPageState();
}

class _LearningPageState extends State<LearningPage> {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    int index = appState.learn_index;
    int activityIndex = appState.activity_index;
    Widget page;
    var length = appState.learn_cards.length;
    if (index >= appState.learn_cards.length) {
      index = 0;
      executeAfterBuild();
      return const Placeholder();
    }
    var types = appState.learn_cards[index].types;
    switch (types[activityIndex]) {
      //switch (types[_random.nextInt(types.length)]) {
      //switch ("dialogue"){
      case "speaking":
        page = SpeakQuestionPage(
            card: appState.learn_cards[index],
            auth_token: widget.auth_token,
            learn: true);
        break;
      case "fill":
        page = FillQuestionPage(
            card: appState.learn_cards[index],
            auth_token: widget.auth_token,
            learn: true);
        break;
      case "dictation":
        page = DictationQuestionPage(
            card: appState.learn_cards[index],
            auth_token: widget.auth_token,
            learn: true);
        break;
      case "dialogue":
        page = DialoguePage(
            card: appState.learn_cards[index],
            auth_token: widget.auth_token,
            learn: true);
        break;
      case "translation":
        page = SimpleQuestionPage(
            card: appState.learn_cards[index],
            auth_token: widget.auth_token,
            name: widget.course_name,
            learn: true);
        break;
      case "word_order":
        page = SortPage(
            card: SortCard.fromActivityCard(appState.learn_cards[index]),
            auth_token: widget.auth_token,
            learn: true);
        break;
      default:
        throw UnimplementedError('no widget for');
    }
    return Stack(
      children: [
        Image.asset(
          "lib/assets/bg.jpg",
          fit: BoxFit.cover,
          height: double.infinity,
          width: double.infinity,
          scale: 1,
        ),
        Column(
          children: [
            GlassmorphicFlexContainer(
              blur: 20,
              border: 2,
              borderRadius: 20,
              linearGradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    const Color(0xFFffffff).withOpacity(0.1),
                    const Color(0xFFFFFFFF).withOpacity(0.05),
                  ],
                  stops: const [
                    0.1,
                    1,
                  ]),
              borderGradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  const Color(0xFFffffff).withOpacity(0.5),
                  const Color((0xFFFFFFFF)).withOpacity(0.5),
                ],
              ),
              child: Stack(
                children: [
                  LayoutBuilder(builder: (context, constraints) {
                    return GlassmorphicContainer(
                        width: double.infinity,
                        height: constraints.maxWidth > 600 ? 65 : 100,
                        blur: 20,
                        border: 2,
                        borderRadius: 20,
                        linearGradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              const Color(0xFFffffff).withOpacity(0.1),
                              const Color(0xFFFFFFFF).withOpacity(0.05),
                            ],
                            stops: const [
                              0.1,
                              1,
                            ]),
                        borderGradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            const Color(0xFFffffff).withOpacity(0.5),
                            const Color((0xFFFFFFFF)).withOpacity(0.5),
                          ],
                        ),
                        child: null);
                  }),
                  Scaffold(
                    backgroundColor: Colors.transparent,
                    appBar: AppBar(
                      iconTheme: const IconThemeData(
                        color: Colors.white, //change your color here
                      ),
                      backgroundColor: Colors.transparent,
                      title: const Text(
                        "Učení",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                    body: Column(children: [
                      Expanded(child: Container(
                          padding: const EdgeInsets.fromLTRB(0, 100, 0, 0),
                          child: Center(child: page)),),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: LinearProgressBar(
                          maxSteps: types.length,
                          progressType: LinearProgressBar
                              .progressTypeDots, // Use Dots progress
                          currentStep: activityIndex,
                          dotsSpacing: EdgeInsets.only(right: 7),
                          progressColor: Colors.greenAccent,
                          backgroundColor: Colors.grey,
                        ),),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                        child: LinearProgressBar(
                          maxSteps: length,
                          progressType: LinearProgressBar
                              .progressTypeDots, // Use Dots progress
                          currentStep: index,
                          dotsSpacing: EdgeInsets.only(right: 7),
                          progressColor: Colors.pinkAccent,
                          backgroundColor: Colors.grey,
                        ),),
                    ]),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  Future<void> executeAfterBuild() async {
    await Future.delayed(Duration.zero);
    Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => MyHomePage(
              auth_token: widget.auth_token,
              course_id: widget.course_id,
              course_name: widget.course_name,
            )));
  }
}
