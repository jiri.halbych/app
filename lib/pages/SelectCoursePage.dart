import 'dart:convert';

import 'package:flag/flag_widget.dart';
import 'package:flutter/material.dart';
import 'package:glassmorphism/glassmorphism.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import '../Storage.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import '../MyAppState.dart';
import '../main.dart';

class Course {
  final int id;
  final String name;
  final String locale;

  const Course({required this.id, required this.name, required this.locale});

  static List<Course> fromJson(Map<String, dynamic> json) {
    List<Course> courses = List<Course>.empty(growable: true);
    json["data"].forEach((course) {
      Course kurz = Course(
          id: course['id'], name: course['name'], locale: course['locale']);
      courses.add(kurz);
    });
    return courses;
  }
}

class SelectCoursePage extends StatefulWidget {
  final String auth_token;

  const SelectCoursePage({super.key, required this.auth_token});

  @override
  State<SelectCoursePage> createState() => _CourseMapPageState();
}

class _CourseMapPageState extends State<SelectCoursePage> {
  Future<List<Course>> fetchCourses(authToken) async {
    final response = await http.get(
      Uri.parse('https://cognoro.xyz/courses'),
      headers: <String, String>{'authorization': authToken},
    );

    if (response.statusCode == 200) {
      return Course.fromJson(jsonDecode(response.body));
    } else {
      throw Exception('Failed to load card');
    }
  }

  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    appState.getReviews(widget.auth_token);
    appState.decks = null;
    var storage = Storage();
    var futurecourses = fetchCourses(widget.auth_token);
    return FutureBuilder<List<Course>>(
        future: futurecourses,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Widget> courses = List.empty(growable: true);
            for (var course in snapshot.data!) {
              var option = Material(
                type: MaterialType.transparency,
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: InkWell(
                    customBorder: const BeveledRectangleBorder(),
                    child: Ink(
                      width: 500,
                      height: 90,
                      decoration: ShapeDecoration(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(25.0)),
                          color: Colors.pinkAccent),
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(course.name,
                                    style: const TextStyle(
                                        color: Colors.white, fontSize: 25))),
                            Expanded(
                                child: Flag.fromString(
                                    course.locale.split('-')[1],
                                    height: 100,
                                    width: null)),
                          ],
                        ),
                      ),
                    ),
                    onTap: () async {
                      await storage.writeCourseId(course.id);
                      await storage.writeCourseLocale(course.locale);
                      await storage.writeCourseName(course.name);
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => MyHomePage(
                            auth_token: widget.auth_token,
                            course_id: course.id.toString(),
                        course_name: course.name,),
                      ));
                    },
                  ),
                ),
              );
              courses.add(option);
            }
            return Stack(children: [
              Image.asset(
                "lib/assets/bg.jpg",
                fit: BoxFit.cover,
                height: double.infinity,
                width: double.infinity,
                scale: 1,
              ),
              Column(children: [
                GlassmorphicFlexContainer(
                  blur: 20,
                  border: 2,
                  borderRadius: 20,
                  linearGradient: LinearGradient(
                      begin: Alignment.topLeft,
                      end: Alignment.bottomRight,
                      colors: [
                        const Color(0xFFffffff).withOpacity(0.1),
                        const Color(0xFFFFFFFF).withOpacity(0.05),
                      ],
                      stops: const [
                        0.1,
                        1,
                      ]),
                  borderGradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      const Color(0xFFffffff).withOpacity(0.5),
                      const Color((0xFFFFFFFF)).withOpacity(0.5),
                    ],
                  ),
                  child: Column(children: [
                    Expanded(child:
                    SingleChildScrollView(
                        child: Center(
                            child: Column(
                      children: courses,
                    ))),),
                    GlassmorphicContainer(
                        width: double.infinity,
                        height: 65,
                        blur: 20,
                        border: 2,
                        borderRadius: 20,
                        linearGradient: LinearGradient(
                            begin: Alignment.topLeft,
                            end: Alignment.bottomRight,
                            colors: [
                              const Color(0xFFffffff).withOpacity(0.1),
                              const Color(0xFFFFFFFF).withOpacity(0.05),
                            ],
                            stops: const [
                              0.1,
                              1,
                            ]),
                        borderGradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            const Color(0xFFffffff).withOpacity(0.5),
                            const Color((0xFFFFFFFF)).withOpacity(0.5),
                          ],
                        ),
                        child: const Center(
                          child: Text("Vyber kurz",
                              style: TextStyle(
                                  fontFamily: "Roboto",
                                  fontWeight: FontWeight.normal,
                                  decoration: TextDecoration.none,
                                  color: Colors.white,
                                  fontSize: 25)),
                        ))
                  ]),
                ),
              ]),
            ]);
          }
          return Stack(children: [
            Image.asset(
              "lib/assets/bg.jpg",
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
              scale: 1,
            ),
            Column(children: [
              GlassmorphicFlexContainer(
                blur: 20,
                border: 2,
                borderRadius: 20,
                linearGradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    colors: [
                      const Color(0xFFffffff).withOpacity(0.1),
                      const Color(0xFFFFFFFF).withOpacity(0.05),
                    ],
                    stops: const [
                      0.1,
                      1,
                    ]),
                borderGradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight,
                  colors: [
                    const Color(0xFFffffff).withOpacity(0.5),
                    const Color((0xFFFFFFFF)).withOpacity(0.5),
                  ],
                ),
                child: Column(children: [
                  Center(
                  child: LoadingAnimationWidget.halfTriangleDot(
                  size: 200, color: Colors.pinkAccent,
                  ),),
                  GlassmorphicContainer(
                      width: double.infinity,
                      height: 65,
                      blur: 20,
                      border: 2,
                      borderRadius: 20,
                      linearGradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            const Color(0xFFffffff).withOpacity(0.1),
                            const Color(0xFFFFFFFF).withOpacity(0.05),
                          ],
                          stops: const [
                            0.1,
                            1,
                          ]),
                      borderGradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          const Color(0xFFffffff).withOpacity(0.5),
                          const Color((0xFFFFFFFF)).withOpacity(0.5),
                        ],
                      ),
                      child: const Center(
                        child: Text("Vyber kurz",
                            style: TextStyle(
                                fontFamily: "Roboto",
                                fontWeight: FontWeight.normal,
                                decoration: TextDecoration.none,
                                color: Colors.white,
                                fontSize: 25)),
                      ))
                ]),
              ),
            ]),
          ]);
          });
  }
}
