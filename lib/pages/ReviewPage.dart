import 'package:cognoro/pages/review/DialoguePage.dart';
import 'package:cognoro/pages/review/DictationQuestionPage.dart';
import 'package:cognoro/pages/review/FillQuestionPage.dart';
import 'package:cognoro/pages/review/SimpleQuestionPage.dart';
import 'package:cognoro/pages/review/SortPage.dart';
import 'package:cognoro/pages/review/SpeakQuestionPage.dart';
import 'package:flutter/material.dart';
import 'package:glassmorphism/glassmorphism.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:provider/provider.dart';
import '../MyAppState.dart';
import '../models/ActivityCard.dart';
import "dart:math";

class ReviewPage extends StatefulWidget {
  final String auth_token;
  final String course_name;

  const ReviewPage({super.key, required this.auth_token, required this.course_name});

  @override
  State<ReviewPage> createState() => _ReviewPageState();
}

class _ReviewPageState extends State<ReviewPage> {
  @override
  Widget build(BuildContext context) {
    var appState = context.watch<MyAppState>();
    int index = appState.review_index;
    Widget page;
    var futurecards = appState.fetchReviewDeck(widget.auth_token);
    return FutureBuilder(
        future: futurecards,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (index >= appState.review_cards.length) {
              return const Text("Žádné karty k opakování",
                  style: TextStyle(color: Colors.white, fontSize: 20));
            }
            var types = appState.review_cards[index].types;
            final random = Random();
            switch (types[random.nextInt(types.length)]) {
              //switch ("dialogue"){
              case "speaking":
                page = SpeakQuestionPage(
                    card: appState.review_cards[index],
                    auth_token: widget.auth_token,
                    learn: false);
                break;
              case "fill":
                page = FillQuestionPage(
                    card: appState.review_cards[index],
                    auth_token: widget.auth_token,
                    learn: false);
                break;
              case "dictation":
                page = DictationQuestionPage(
                    card: appState.review_cards[index],
                    auth_token: widget.auth_token,
                    learn: false);
                break;
              case "dialogue":
                page = DialoguePage(
                    card: appState.review_cards[index],
                    auth_token: widget.auth_token,
                    learn: false);
                break;
              case "translation":
                page = SimpleQuestionPage(
                    card: appState.review_cards[index],
                    auth_token: widget.auth_token,
                    name: widget.course_name,
                    learn: false);
                break;
              case "word_order":
                page = SortPage(
                    card:
                        SortCard.fromActivityCard(appState.review_cards[index]),
                    auth_token: widget.auth_token,
                    learn: false);
                break;
              default:
                throw UnimplementedError('no widget for');
            }
            return Stack(
              children: [
                Image.asset(
                  "lib/assets/bg.jpg",
                  fit: BoxFit.cover,
                  height: double.infinity,
                  width: double.infinity,
                  scale: 1,
                ),
                Column(
                  children: [
                    GlassmorphicFlexContainer(
                      blur: 20,
                      border: 2,
                      borderRadius: 20,
                      linearGradient: LinearGradient(
                          begin: Alignment.topLeft,
                          end: Alignment.bottomRight,
                          colors: [
                            const Color(0xFFffffff).withOpacity(0.1),
                            const Color(0xFFFFFFFF).withOpacity(0.05),
                          ],
                          stops: const [
                            0.1,
                            1,
                          ]),
                      borderGradient: LinearGradient(
                        begin: Alignment.topLeft,
                        end: Alignment.bottomRight,
                        colors: [
                          const Color(0xFFffffff).withOpacity(0.5),
                          const Color((0xFFFFFFFF)).withOpacity(0.5),
                        ],
                      ),
                      child: Stack(
                        children: [
                          LayoutBuilder(builder: (context, constraints) {
                            return GlassmorphicContainer(
                                width: double.infinity,
                                height: constraints.maxWidth > 600 ? 65 : 100,
                                blur: 20,
                                border: 2,
                                borderRadius: 20,
                                linearGradient: LinearGradient(
                                    begin: Alignment.topLeft,
                                    end: Alignment.bottomRight,
                                    colors: [
                                      const Color(0xFFffffff).withOpacity(0.1),
                                      const Color(0xFFFFFFFF).withOpacity(0.05),
                                    ],
                                    stops: const [
                                      0.1,
                                      1,
                                    ]),
                                borderGradient: LinearGradient(
                                  begin: Alignment.topLeft,
                                  end: Alignment.bottomRight,
                                  colors: [
                                    const Color(0xFFffffff).withOpacity(0.5),
                                    const Color((0xFFFFFFFF)).withOpacity(0.5),
                                  ],
                                ),
                                child: null);
                          }),
                          Scaffold(
                            backgroundColor: Colors.transparent,
                            appBar: AppBar(
                              iconTheme: const IconThemeData(
                                color: Colors.white, //change your color here
                              ),
                              backgroundColor: Colors.transparent,
                              title: const Text(
                                "Opakování",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                            body: Container(
                                padding:
                                    const EdgeInsets.fromLTRB(0, 100, 0, 0),
                                child: Center(child: page)),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            );
          } else {
            return Center(
              child: LoadingAnimationWidget.halfTriangleDot(
                size: 200,
                color: Colors.pinkAccent,
              ),
            );
            //return Text("Žádné karty k opakování",style: TextStyle(color: Colors.white, fontSize: 20));
          }
        });
  }
}
